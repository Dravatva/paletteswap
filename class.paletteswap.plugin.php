<?php if (!defined('APPLICATION')) exit();

/**
 * ButtonBar Plugin
 * 
 * @author Tim Gunter <tim@vanillaforums.com>
 * @copyright 2003 Vanilla Forums, Inc
 * @license http://www.opensource.org/licenses/gpl-2.0.php GPL
 * @package Addons
 */

$PluginInfo['PaletteSwap'] = array(
   'Name' => 'Palette Swapper',
   'Description' => 'Allows users to select different color palettes for their theme.',
   'Version' => '0',
   'MobileFriendly' => TRUE,
   'RequiredApplications' => array('Vanilla' => '2.0'),
   'RequiredTheme' => FALSE, 
   'RequiredPlugins' => FALSE,
   'Author' => "Joey Bach Hardie",
   'AuthorEmail' => 'joeymacguffin@gmail.com',
   'AuthorUrl' => 'nope'
);

class PaletteSwapPlugin extends Gdn_Plugin {   

   public function Base_Render_Before($Sender) {
      $Session = Gdn::Session();
      $Palette = $Session->GetPreference('Palette');
	 if (is_object($Sender->Head) && $Palette) {
	    $Theme = C('Garden.Theme','default');
	    $Sender->AddCssFile("custom_".$Palette.".css");
	 }
	 else {
	    $Sender->AddCssFile("custom_default.css");
	 }
   }

   public function PluginController_Palette_Create($Sender, $PaletteName) {
      ini_set('display_errors', 'On');
      Gdn::Session()->SetPreference('Palette', $PaletteName);
      Redirect($_SERVER['HTTP_REFERER']);
   }

   public function PaletteButton() {
      if (Gdn::ApplicationManager()->CheckApplication('Vanilla')) {
         echo '<span class="ToggleFlyout">';
         echo Anchor(Sprite('SpPicture', 'Sprite Sprite16').Wrap(T('Palette'), 'em'), '/plugins/PaletteSwap/', 'MeButton FlyoutButton', array('title' => T('Palette')));
         echo Sprite('SpFlyoutHandle', 'Arrow');
         echo '<div class="Flyout MenuItems"><ul>';
	 $List = C('Plugins.PaletteSwap.Palettes');
	 foreach ($List as $Item) {
	    echo Wrap(Anchor($Item,'plugin/palette/'.$Item),'li');
	 }
	 echo '</ul></div></span>';
      } 
   }
}
